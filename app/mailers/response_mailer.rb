class ResponseMailer < ApplicationMailer
  default from: 'enquiries@rhionromany.com'

  def response_sent(response)
    @response = response
    mail(to: Admin.first.email, from: @response.writer.email, subject: 'Response Received')
  end
end
