class EnquiryMailer < ApplicationMailer
  default from: 'enquiries@rhionromany.com'

  def enquiry_made(user, enquiry)
    @user = user
    @enquiry = enquiry
    mail(to: Admin.first.email, from: @user.email, subject: 'Enquiry Received')
  end
end
