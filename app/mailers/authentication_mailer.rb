class AuthenticationMailer < Devise::Mailer
  include Devise::Controllers::UrlHelpers
  default from: 'accounts@rhionromany.com'
  layout 'mailer'
  helper :application
end
