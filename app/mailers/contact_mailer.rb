class ContactMailer < ApplicationMailer

  def send_message(message)
    @message = message
    mail(to: 'rhion@rhionromany.com', from: @message.email, subject: @message.subject)
  end
end
