//= require packery.pkgd.min
//= require imagesloaded.pkgd.min

if ($('.packery').length > 0) {
    var grid = $('#collection-items').packery({
        percentPosition: true
    });
    grid.imagesLoaded().progress(function() {
        grid.packery();
    });

    var editorialContent = $('.editorial > .content');
    var collapseEditorialButton = $('#collapse-editorial');
    var expandEditorialButton = $('#expand-editorial');

   collapseEditorialButton.click(function() {
        editorialContent.slideUp(500, function() {
            collapseEditorialButton.hide();
            expandEditorialButton.show();
        });
    });

    expandEditorialButton.click(function() {
        editorialContent.slideDown(500);
        expandEditorialButton.hide();
        collapseEditorialButton.show();
    });
}