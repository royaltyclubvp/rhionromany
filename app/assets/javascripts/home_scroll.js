$(document).ready(function() {
    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

    new ScrollMagic.Scene({triggerElement: "#first"})
        .setTween("#first > div", {y: "80%", ease: Linear.easeNone})
        .addTo(controller);

    new ScrollMagic.Scene({triggerElement: "#second"})
        .setTween("#second > div", {y: "80%", ease: Linear.easeNone})
        .addTo(controller);

    new ScrollMagic.Scene({triggerElement: "#third"})
        .setTween("#third > div", {y: "80%", ease: Linear.easeNone})
        .addTo(controller);


    new ScrollMagic.Scene({triggerElement: "#fourth"})
        .setTween("#fourth > div", {y: "40%", ease: Linear.easeNone})
        .addTo(controller)
        .duration("100%");
});