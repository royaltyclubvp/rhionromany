var initiate_checkout = function() {
    var errorContainer = $('#checkout-errors');
    var addressContainer = $('#address-container');
    var loadingContainer = $('#loading-container');
    var orderForm = $('.order-form');
    var creditCardForm = $('#credit-card-form');

    var showLoading = function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        loadingContainer.show('slow');
        $('body').css('overflow', 'hidden');
    };

    var hideLoading = function() {
        $('body').css('overflow', 'scroll');
        loadingContainer.hide('fast');
    };

    var generateErrorCode = function() {
        return 'error-' + Date.now();
    };

    var addError = function(msg, id) {
        errorContainer.append(
            $('<div>').append(msg).attr('id', id).addClass('alert alert-error fade in')
        );
    };

    var removeError = function(error_code) {
        $('#' + error_code).remove();
    };

    var displayErrors = function() {
        errorContainer.prepend(
            $('<div>').append('There are errors that must be corrected before proceeding').addClass('alert alert-info fade in')
        );
        errorContainer.show();
    };

    var validateCreditCardForm = function() {
        var status = true;
        var month = $('#expMonth');
        var year = $('#expYear');
        var ccNo = $('#ccNo');
        var error_code;
        // Check if Fields are Empty
        creditCardForm.find('input[type=text]').each(function() {
            if ($(this).val() == '') {
                $(this).addClass('value-missing-error');
                error_code = generateErrorCode();
                var field_label = $(this).parent().find('label').html();
                if (field_label == undefined) {
                    field_label = $(this).parent().parent().parent().find('label').html();
                }
                addError('The required field, ' + field_label + ', has not been filled out.', error_code);
                var self = $(this);
                $(this).one('change', function() {
                    removeError(error_code);
                    self.removeClass('value-missing-error');
                });
                status = false;
            }
        });

        // Check length of month & year fields
        if(month.val().length != 2) {
            month.addClass('invalid-format-error');
            error_code = generateErrorCode();
            addError('The month should be entered in a two digit format [e.g. 03]', error_code);
            var self = month;
            month.on('change', function() {
                if(self.val().length == 2) {
                    self.removeClass('invalid-format-error');
                    removeError(error_code);
                    self.off('change');
                }
            });
            status = false;
        }
        if(year.val().length != 4) {
            year.addClass('invalid-format-error');
            error_code = generateErrorCode();
            addError('The year should be entered in a four digit format [e.g. 2015]', error_code);
            year.on('change', function() {
                if(year.val().length == 4) {
                    year.removeClass('invalid-format-error');
                    removeError(error_code);
                    year.off('change');
                }
            });
            status = false;
        }

        // Check length of Credit Card Value for Validity
        if(ccNo.val().length != 13 && ccNo.val().length != 16 && ccNo.val().length != 0) {
            ccNo.addClass('invalid-format-error');
            error_code = generateErrorCode();
            addError('The credit card number you entered is not valid', error_code);
            ccNo.on('change', function() {
                if(ccNo.val().length == 13 || ccNo.val().length == 16) {
                    ccNo.removeClass('invalid-format-error');
                    removeError(error_code);
                    ccNo.off('change');
                }
            });
            status = false;
        }

        return status;
    };

    var disableSensitiveFields = function() {
        $('#ccNo, #expMonth, #expYear, #cvv').attr('disabled', true);
    };

    var validateAddressForm = function() {
        var status = true;
        var addr2 = $('.address_2');
        var state = $('.state');
        var zipCode = $('.zip_code');
        var country = $('.country');
        var stateRequired = ["AR", "AU", "BG", "CA", "CN", "CY", "EG", "FR", "IN", "ID", "IT", "JP", "MY", "MX", "NL", "PA", "PH", "PL", "RO", "RU", "RS", "SG", "ZA", "ES", "SE", "TH", "TR", "GB", "US"];
        var zipRequired = ["AR", "AU", "BG", "CA", "CN", "CY", "EG", "FR", "IN", "ID", "IT", "JP", "MY", "MX", "NL", "PA", "PH", "PL", "RO", "RU", "RS", "SG", "ZA", "ES", "SE", "TH", "TR", "GB", "US"];
        var addr2Required = ["CHN", "JPN","RUS"];
        var error_code;

        // Check for Generally Required Items
        $('#cardholder_name, .address_1, .city, .country, #email_address').each(function() {
            if ($(this).val() == '') {
                $(this).addClass('value-missing-error');
                var error_code = generateErrorCode();
                var field_label = $(this).parent().find('label').html();
                addError('The required field, ' + field_label + ', has not been filled out', error_code);
                var self = $(this);
                $(this).one('change', function() {
                    removeError(error_code);
                    self.removeClass('value-missing-error');
                });
                status = false;
            }
        });

        // If State is Required, Check for its presence
        if (isInArray(country.val(), stateRequired)) {
            if (state.val() == '') {
                state.addClass('value-missing-error');
                error_code = generateErrorCode();
                addError('Your state is required', error_code);
                state.one('change', function() {
                    removeError(error_code);
                    state.removeClass('value-missing-error');
                });
                status = false;
            }
        }

        // If Zip is Required, check for its presence
        if (isInArray(country.val(), zipRequired)) {
            if (zipCode.val() == '') {
                zipCode.addClass('value-missing-error');
                error_code = generateErrorCode();
                addError('Your zip code is required', error_code);
                zipCode.one('change', function() {
                    removeError(error_code);
                    zipCode.removeClass('value-missing-error');
                });
                status = false;
            }
        }

        // If Addr2 is Required, check for its presence
        if (isInArray(country.val(), addr2Required)) {
            if (addr2.val() == '') {
                addr2.addClass('value-missing-error');
                error_code = generateErrorCode();
                addError('An entry for Line 2 of your Street Address is required', error_code);
                addr2.one('change', function() {
                    removeError(error_code);
                    addr2.removeClass('value-missing-error');
                });
                status = false;
            }
        }
        return status;
    };

    var disableAddressForm = function() {
        addressContainer.find('input').attr('readonly', true);
        addressContainer.find('select').attr('disabled', true);
    };

    var enableAddressForm = function() {
        addressContainer.find('input').attr('readonly', false);
        addressContainer.find('select').attr('disabled', false);
    };

    var copyShippingAddressToBilling = function() {
        var street_address_1, street_address_2, city, state, country, zip_code;
        street_address_1 = $('#order_address_attributes_street_address_1').val();
        street_address_2 = $('#order_address_attributes_street_address_2').val();
        city = $('#order_address_attributes_city').val();
        state = $('#order_address_attributes_state').val();
        zip_code = $('#order_address_attributes_zip_code').val();
        country = $('#order_address_attributes_country').val();
        $('#order_billing_street_address_1').val(street_address_1);
        $('#order_billing_street_address_2').val(street_address_2);
        $('#order_billing_city').val(city);
        $('#order_billing_state').val(state);
        $('#order_billing_zip_code').val(zip_code);
        $('#order_billing_country').val(country);
    };

    var successCallBack = function(data) {
        // Assign Token returned from server & other variables that need to be sent to the server
        $('#order_token').val(data.response.token.token);
        $('#order_credit_card').val(data.response.paymentMethod.cardNum);
        disableSensitiveFields();
        enableAddressForm();
        orderForm.submit();
    };

    var errorCallBack = function(data) {
        var error_code = generateErrorCode();
        if (data.errorCode === 200) {
            addError('There was a problem communicating with the payment server. Please try again', error_code);
        } else if (data.errorCode === 400 || data.errorCode === 401) {
            addError('Please enter all required fields before attempting to authorize payment', error_code);
        } else if (data.errorCode === 300 || data.errorCode === 500) {
            addError('An error occurred. Please close this page and try again in 30 minutes', error_code);
        }
        displayErrors();
    };

    var tokenRequest = function() {
        var args = {
            sellerId: two_checkout_sellerId,
            publishableKey: two_checkout_publishableKey,
            ccNo: $('#ccNo').val(),
            cvv: $('#cvv').val(),
            expMonth: $('#expMonth').val(),
            expYear: $('#expYear').val()
        };

        // Call 2Checkout Token request function
        TCO.requestToken(successCallBack, errorCallBack, args);
    };

    // Load 2Checkout Library
    jQuery.getScript("https://www.2checkout.com/checkout/api/2co.min.js", function() {
        TCO.loadPubKey(two_checkout_environment);
    });

    // Copy Address Handler
    $('#same-as-shipping').click(function(e) {
        e.preventDefault();
        copyShippingAddressToBilling();
    });

    // Submit Form Handler
    $('#authorize-transaction-button').click(function() {
        orderForm.find('span.error, input.error').remove();
        if (validateAddressForm()) {
            disableAddressForm();
            if (validateCreditCardForm()) {
                showLoading();
                tokenRequest();
            } else {
                displayErrors();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
        } else {
            displayErrors();
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
        return false;
    });
};