//= require jquery2
//= require jquery_ujs
//= require js.cookie
//= require jstz
//= require browser_timezone_rails/set_time_zone
//= require tether
//= require bootstrap
//= require jquery.elevate-zoom
//= require polyfill.object-fit
//= require helper_functions
//= require autonumeric
//= require collections
//= require checkout

$(function() {
    $('a[href*=\\#]:not([href=\\#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('.zoom-inner').elevateZoom({
        zoomType: 'inner',
        cursor: 'crosshair'
    });

    objectFit.polyfill({
        selector: 'img', // this can be any CSS selector
        fittype: 'contain', // either contain, cover, fill or none
        disableCrossDomain: 'false' // either 'true' or 'false' to not parse external CSS files.
    });

    $('.collection-item').click(function() {
        window.location = $(this).data('location');
    });

    if (document.getElementsByClassName('order-form').length) {
        initiate_checkout();
    }
});

