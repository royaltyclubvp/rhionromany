# == Schema Information
#
# Table name: enquiries
#
#  id                 :integer          not null, primary key
#  collection_item_id :integer
#  user_id            :integer
#  message            :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :integer          default(0)
#
# Indexes
#
#  index_enquiries_on_collection_item_id  (collection_item_id)
#  index_enquiries_on_user_id             (user_id)
#

class Enquiry < ActiveRecord::Base
  # Set status as enum
  enum status: %i(opened authorized closed)

  # Relationships
  #
  # Has many RESPONSES
  has_many :responses, inverse_of: :enquiry

  # Sent by a USER
  belongs_to :user, inverse_of: :enquiries

  # Related to a COLLECTION_ITEM
  belongs_to :collection_item, inverse_of: :enquiries

  # May have one SALE AUTHORIZATION
  has_one :sale_authorization, inverse_of: :enquiry

  # Model Callbacks
  #
  # After Save - Send Email Notification
  after_create :send_email_notification

  # Convenience Methods
  #
  # Enquiry Response URL
  def admin_response_url
    ENV['admin_host'] + '/enquiries/' + self.id.to_s + '/responses/new'
  end

  # Service Methods
  #
  # Send Notification of new Enquiry
  def send_email_notification
    EnquiryMailer.enquiry_made(self.user, self).deliver_now
  end
end
