class ContactSubmission
  include ActiveModel::Model

  # Attributes
  attr_accessor :name, :email, :subject, :message

  # Class Methods
  #
  # Send Message
  def send
    ContactMailer.send_message(self).deliver_now
  end

end
