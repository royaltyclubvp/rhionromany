# == Schema Information
#
# Table name: addresses
#
#  id               :integer          not null, primary key
#  street_address_1 :string
#  street_address_2 :string
#  city             :string
#  state            :string
#  zip_code         :string
#  country          :string
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_addresses_on_user_id  (user_id)
#

class Address < ActiveRecord::Base
  # Relationships
  #
  # Belongs to a USER
  belongs_to :user, inverse_of: :address

  # Has many ORDERS
  has_many :orders, inverse_of: :address

  # Model Callbacks
  before_save :set_zip_code_if_missing

  # Instance Methods
  def set_zip_code_if_missing
    self.zip_code ||= '00000'
  end

end
