# == Schema Information
#
# Table name: collection_items
#
#  id            :integer          not null, primary key
#  name          :string
#  collection_id :integer
#  image_id      :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  slug          :string
#  item_for_sale :boolean          default(FALSE)
#
# Indexes
#
#  index_collection_items_on_collection_id  (collection_id)
#  index_collection_items_on_slug           (slug) UNIQUE
#

class CollectionItem < ActiveRecord::Base
  # File Management
  attachment :image,
             content_type: 'image/jpeg'

  # Attach Friendly URL
  extend FriendlyId
  friendly_id :name, use: :slugged

  # Relationships
  #
  # Is part of a COLLECTION
  belongs_to :collection,
      inverse_of: :collection_items

  # Has received many ENQUIRIES
  has_many :enquiries,
      inverse_of: :collection_item

  # Has many SALE AUTHORIZATIONS
  has_many :sale_authorizations,
           inverse_of: :collection_item

  # Has many ORDERS
  has_many :orders,
      inverse_of: :collection_item
end
