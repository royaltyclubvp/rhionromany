# == Schema Information
#
# Table name: orders
#
#  id                    :integer          not null, primary key
#  sale_authorization_id :integer
#  collection_item_id    :integer
#  address_id            :integer
#  user_id               :integer
#  quantity              :integer
#  bra_size              :string
#  bust_size             :string
#  waist_size            :string
#  hip_size              :string
#  additional_info       :text
#  transaction_id        :string
#  payment_cents         :integer
#  status                :integer          default(0)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  order_number          :string
#  credit_card           :string
#
# Indexes
#
#  index_orders_on_address_id             (address_id)
#  index_orders_on_collection_item_id     (collection_item_id)
#  index_orders_on_sale_authorization_id  (sale_authorization_id)
#  index_orders_on_user_id                (user_id)
#

class Order < ActiveRecord::Base
  # Set status as enum
  enum status: %i(submitted paid fulfilled cancelled)

  # Apply Money Adapter
  monetize :payment_cents

  # Virtual Readers
  #
  # Billing Address Accessors
  attr_accessor :billing_street_address_1, :billing_street_address_2,
                :billing_city, :billing_state, :billing_zip_code, :billing_country,
                :cardholder_name, :email_address, :phone_number
  # Currency Accessor
  attr_accessor :currency

  # Token Accessor
  attr_accessor :token

  # Relationships
  #
  # Created from a SALE AUTHORIZATION
  belongs_to :sale_authorization, inverse_of: :order

  # Includes a COLLECTION ITEM
  belongs_to :collection_item, inverse_of: :orders

  # Submitted by a USER
  belongs_to :user, inverse_of: :orders

  # Has one ADDRESS
  belongs_to :address, inverse_of: :orders

  # Has TRANSACTION_ERRORS
  has_many :transaction_errors, inverse_of: :order

  # Data Handlers
  #
  # Address Attribute Handlers
  accepts_nested_attributes_for :address,
                                reject_if: :all_blank


  # Model Callbacks
  before_save :set_collection_item_if_missing, :set_user_id_if_missing
  after_create :purchase


  # Callback Methods
  #
  # Set Collection Item ID
  def set_collection_item_if_missing
    self.collection_item ||= self.sale_authorization.collection_item
  end

  # Set User ID
  def set_user_id_if_missing
    self.user ||= self.sale_authorization.user
  end

  # Instance Methods
  #
  # Create Unique Identifier for Merchant Order ID
  def merchant_id
    "I#{self.collection_item_id}UID#{self.user_id}"
  end

  # Checkout Parameters
  def checkout_params
    params = {
        merchantOrderId: merchant_id,
        token: self.token,
        currency: self.currency,
        lineItems: line_items,
        billingAddr: billing_attributes
    }
  end

  # Create Purchase
  def purchase
    begin
      result = Twocheckout::Checkout.authorize(checkout_params)
      self.transaction_id = result["transactionId"].to_s
      self.order_number = result["orderNumber"].to_s
      self.paid!
      self.sale_authorization.completed!
      true
    rescue Twocheckout::TwocheckoutError => e
      self.transaction_errors.create(message: e.message)
      false
    end
  end


  # Convenience Methods
  #
  # Return Hash of Billing Address Attributes
  def billing_attributes
    attributes = {
        name: self.cardholder_name,
        addrLine1: self.billing_street_address_1,
        addrLine2: self.billing_street_address_2,
        city: self.billing_city,
        state: self.billing_state,
        zipCode: self.billing_zip_code,
        country: self.billing_country,
        email: self.email_address,
        phoneNumber: self.phone_number
    }
  end

  def shipping_attributes
    attributes = {
        name: self.user.name,
        addrLine1: self.address.street_address_1,
        addrLine2: self.address.street_address_2,
        city: self.address.city,
        state: self.address.state,
        zipCode: self.address.zip_code,
        country: self.address.country
    }
  end

  # Return Hash of Line Items
  def line_items
    attributes = [
        {
            type: 'product',
            name: "#{self.collection_item.name} - #{self.collection_item.collection.name} Collection",
            quantity: self.quantity,
            price: self.payment_cents/100.to_f,
            tangible: 'Y',
            productId: self.collection_item_id,
            options: [
                {
                    optName: 'Bra Size',
                    optValue: self.bra_size
                },
                {
                    optName: 'Bust Size',
                    optValue: self.bust_size
                },
                {
                    optName: 'Waist Size',
                    optValue: self.waist_size
                },
                {
                    optName: 'Hip Size',
                    optValue: self.hip_size
                },
                {
                    optName: 'Additional Info',
                    optValue: self.additional_info
                }
            ]
        }
    ]
  end
end
