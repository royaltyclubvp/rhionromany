# == Schema Information
#
# Table name: transaction_errors
#
#  id         :integer          not null, primary key
#  order_id   :integer
#  message    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_transaction_errors_on_order_id  (order_id)
#

class TransactionError < ActiveRecord::Base
  # Relationships
  #
  # Belong to an ORDER
  belongs_to :order, inverse_of: :transaction_errors
end
