# == Schema Information
#
# Table name: collections
#
#  id         :integer          not null, primary key
#  name       :string
#  year       :integer
#  slug       :string
#  available  :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  ordered    :boolean          default(FALSE)
#
# Indexes
#
#  index_collections_on_slug  (slug) UNIQUE
#

class Collection < ActiveRecord::Base
  # Scopes
  #
  # Available collections
  scope :available, -> { where(available: true) }
  scope :desc, -> {available.order(year: :desc)}

  # Attach Friendly URL
  extend FriendlyId
  friendly_id :name, use: :slugged

  # Relationships
  #
  # Contains many ITEMS
  has_many :collection_items, -> {order("created_at asc")},
      inverse_of: :collection,
      dependent: :delete_all

  # Convenience Methods
  #
  # Display 'Yes' or 'No' to represent available attribute
  def published?
    if self.available
      'Yes'
    else
      'No'
    end
  end
end
