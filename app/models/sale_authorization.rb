# == Schema Information
#
# Table name: sale_authorizations
#
#  id                 :integer          not null, primary key
#  collection_item_id :integer
#  enquiry_id         :integer
#  price_cents        :integer
#  status             :integer          default(0)
#  valid_to           :datetime
#  authorization_code :string
#  user_id            :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  message            :text
#
# Indexes
#
#  index_sale_authorizations_on_authorization_code  (authorization_code)
#  index_sale_authorizations_on_collection_item_id  (collection_item_id)
#  index_sale_authorizations_on_enquiry_id          (enquiry_id)
#  index_sale_authorizations_on_user_id             (user_id)
#

class SaleAuthorization < ActiveRecord::Base
  # Apply Money Adapter
  monetize :price_cents

  # Establish status as an enum
  enum status: %i(open completed cancelled)

  # Scopes
  scope :valid, -> {open.where('valid_to > ?', Time.zone.now)}

  # Relationships
  #
  # Related to a COLLECTION_ITEM
  belongs_to :collection_item, inverse_of: :sale_authorizations

  # Sent to a USER
  belongs_to :user, inverse_of: :sale_authorizations

  # Related to an ENQUIRY
  belongs_to :enquiry, inverse_of: :sale_authorization

  # Has an ORDER
  has_one :order, inverse_of: :sale_authorization

  # Model Callbacks
  before_save :generate_code, :set_user_if_missing, :set_collection_item_if_missing

  # Callback Methods
  def generate_code
    self.authorization_code = SecureRandom.urlsafe_base64(8)
  end

  def set_user_if_missing
    self.user ||= self.enquiry.user
  end

  def set_collection_item_if_missing
    self.collection_item ||= self.enquiry.collection_item
  end

  # Convenience Methods
  #
  # Cancel Authorization
  def cancel
    self.cancelled!
    self.enquiry.closed!
  end
end
