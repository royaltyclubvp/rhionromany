module OrdersHelper
  # Set Up Order Object to include New Address Attributes
  def setup_order(order)
    order.build_address ||= Address.new
    order
  end

  # Return Formatted Date According to Status of Order
  def order_status_date(order, element, html_class)
    date, id, label = ''
    case order.status
      when 'submitted'
        date = order.created_at
        id = 'date-submitted'
        label = 'Date Submitted: '
      when 'paid'
        date = order.updated_at
        id = 'date-paid'
        label = 'Date Paid: '
      when 'fulfilled'
        date = order.updated_at
        id = 'date-fulfilled'
        label = 'Date Fulfilled: '
      when 'cancelled'
        date = order.updated_at
        id = 'date-cancelled'
        label = 'Date Cancelled: '
      else
    end
    content_tag(element, "<strong>#{label}</strong>#{date.strftime('%m/%d/%Y')}".html_safe, {class: html_class, id: id})
  end
end
