module ApplicationHelper
  # Set page IDs
  def page(text)
    content_for :page, text
  end

  # Set classes for flash levels
  def flash_class(level)
    case level
      when :notice then 'alert alert-info'
      when :success then 'alert alert-success'
      when :error then 'alert alert-error'
      when :alert then 'alert alert-error'
      else 'alert alert-info'
    end
  end

  # Check if page is current
  def page?(param)
    url = request.path_info
    url.include?(param)
  end

  # Check if request is served by specified controller
  def controller?(c)
    controller.controller_name == c
  end

  # Devise Helpers
  def resource_name
    :user
  end

  def resource
    @user ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

end
