class AccountsController < ApplicationController
  before_action :authenticate_user!
  layout 'content'

  def show

  end

  # Change Password
  def change_password
    if passwords_match?(params[:user][:password], params[:user][:password_confirmation])
      user = current_user
      user.password = params[:user][:password]
      user.save
      redirect_to account_path, notice: 'Your password was successfully changed.'
    else
      redirect_to account_path, error: 'The passwords you entered did not match. Please try again.'
    end
  end

  private

    def passwords_match?(password, password_confirmation)
      password == password_confirmation
    end
end
