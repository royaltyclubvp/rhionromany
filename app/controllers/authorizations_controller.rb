class AuthorizationsController < ApplicationController
  before_action :authenticate_user!
  layout 'content'

  def index
    @user = User.includes(open_authorizations: :collection_item).find(current_user.id)
  end

  def destroy
    SaleAuthorization.find(params[:id]).cancel
    redirect_to account_authorizations_path, notice: 'You have successfully cancelled your purchase request'
  end
end
