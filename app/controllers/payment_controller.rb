class PaymentController < ApplicationController
  before_action :authenticate_user!
  layout 'content'

  def retry
    @order = Order.includes(sale_authorization: :collection_item).find(params[:order_id])
  end

  def authorize
    order = Order.find(params[:order_id])
    order.attributes = payment_params
    if order.purchase
      redirect_to account_order_path(order), notice: 'Your payment has been accepted'
    else
      redirect_to account_order_path(order), notice: 'There was a problem processing your payment'
    end
  end

  def errors
    @order = Order.includes(:transaction_errors, collection_item: :collection).find(params[:order_id])
  end

  private

  def payment_params
    params.require(:order).permit(:cardholder_name, :credit_card, :currency, :email_address, :billing_city, :billing_state,
                                  :billing_street_address_1, :billing_street_address_2, :billing_zip_code, :billing_country, :phone_number,
                                  :token)
  end
end
