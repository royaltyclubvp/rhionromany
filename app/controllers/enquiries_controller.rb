class EnquiriesController < ApplicationController
  before_action :authenticate_user!
  layout 'content'

  # List all open Enquiries
  def index
    @enquiries = current_user.enquiries.opened
  end

  # Delete an Enquiry
  def destroy
    current_user.enquiries.find(params[:id]).destroy
    redirect_to account_enquiries_path
  end

  # Create Enquiry
  def create
    enquiry = Enquiry.new(enquiry_params)
    enquiry.user = current_user
    enquiry.save
    redirect_to account_enquiries_path
  end

  private

    def enquiry_params
      params.require(:enquiry).permit(:collection_item_id, :message)
    end
end
