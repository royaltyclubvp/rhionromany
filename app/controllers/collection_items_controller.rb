class CollectionItemsController < ApplicationController
  layout 'content'

  def show
    @collection_item = CollectionItem.includes(collection: [:collection_items]).friendly.find(params[:id])
    possible_prev_item_no = @collection_item.slug.partition('-').third.to_i - 1
    prev_exists, prev_item, next_exists, next_item = nil
    prev_item = @collection_item.collection.collection_items.find_by(slug: 'look-' + possible_prev_item_no.to_s)
    unless prev_item.nil?
      prev_exists = true
      prev_item = prev_item
    end
    possible_next_item_no = @collection_item.slug.partition('-').third.to_i + 1
    next_item = @collection_item.collection.collection_items.find_by(slug: 'look-' + possible_next_item_no.to_s)
    unless next_item.nil?
      next_exists = true
      next_item = next_item
    end
    if @collection_item.slug.partition('-').first != 'look'
      next_exists = true
      next_item = @collection_item.collection.collection_items.find_by(slug: 'look-1')
    end
    render locals: {
        prev_exists: prev_exists,
        prev_item: prev_item,
        next_exists: next_exists,
        next_item: next_item
    }
  end
end
