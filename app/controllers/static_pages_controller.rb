class StaticPagesController < ApplicationController
  layout 'content', except: [:home]

  def home
    render layout: 'home'
  end

  def contact
    @contact = ContactSubmission.new
  end

  def send_contact
    ContactSubmission.new(message_params).send
    redirect_to contact_path, notice: 'Message sent. You will receive a response as quickly as possible'
  end

  def about
    render layout: 'darkmenu_content'
  end

  private

    def message_params
      params.require(:contact_submission).permit(:message, :email, :subject, :name)
    end
end
