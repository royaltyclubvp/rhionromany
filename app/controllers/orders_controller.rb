class OrdersController < ApplicationController
  before_action :authenticate_user!
  layout 'content'
  include OrdersHelper

  def new
    @sale_authorization = SaleAuthorization.includes(collection_item: :collection).find(params[:authorization_id])
  end

  def create
    Order.create(order_params)
    redirect_to account_order_path(current_user.orders.last)
  end

  def index
    @user = User.includes(orders: :collection_item).find(current_user.id)
  end

  def show
    @order = Order.find(params[:id])
  end

  private

  def order_params
    params.require(:order).permit(:cardholder_name, :credit_card, :currency, :email_address, :additional_info, :billing_city, :billing_state,
    :billing_street_address_1, :billing_street_address_2, :billing_zip_code, :billing_country, :waist_size, :phone_number, :bra_size, :bust_size, :hip_size, :quantity,
    :payment, :token, :collection_item_id, :sale_authorization_id, :user_id, address_attributes: [:street_address_1, :street_address_2, :city, :state, :zip_code, :country])
  end
end
