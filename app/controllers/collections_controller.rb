class CollectionsController < ApplicationController
  layout 'content'

  # Show Collection
  def show
    @collection = Collection.includes(:collection_items).friendly.find(params[:id])
  end

  # Show All Collections
  def index
    @collections = Collection.includes(:collection_items).desc
  end
end
