class ResponsesController < ApplicationController
  before_action :authenticate_user!
  layout 'content'

  def index
    @enquiry = Enquiry.includes(:responses, :collection_item).find(params[:enquiry_id])
  end


  # Create new Response
  def create
    response = Response.new(response_params)
    response.writer = current_user
    response.save
    redirect_to account_enquiry_responses_path(params[:enquiry_id])
  end

  private

    def response_params
      params.require(:response).permit(:enquiry_id, :message)
    end
end
