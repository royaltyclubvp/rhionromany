class Users::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]

  def new
    set_minimum_password_length
    if params[:redirect_to].present?
      collection_item = CollectionItem.includes(:collection).friendly.find(params[:redirect_to])
      store_location_for(:user, collection_item_path(collection_item.collection, collection_item))
    end
    super
  end

  # POST /resource/sign_in
  def create
    super
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
