# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161124171519) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string   "street_address_1"
    t.string   "street_address_2"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "country"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id", using: :btree

  create_table "admins", force: :cascade do |t|
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree

  create_table "collection_items", force: :cascade do |t|
    t.string   "name"
    t.integer  "collection_id"
    t.string   "image_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "slug"
    t.boolean  "item_for_sale", default: false
  end

  add_index "collection_items", ["collection_id"], name: "index_collection_items_on_collection_id", using: :btree
  add_index "collection_items", ["slug"], name: "index_collection_items_on_slug", unique: true, using: :btree

  create_table "collections", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.string   "slug"
    t.boolean  "available"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "ordered",    default: false
  end

  add_index "collections", ["slug"], name: "index_collections_on_slug", unique: true, using: :btree

  create_table "enquiries", force: :cascade do |t|
    t.integer  "collection_item_id"
    t.integer  "user_id"
    t.text     "message"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "status",             default: 0
  end

  add_index "enquiries", ["collection_item_id"], name: "index_enquiries_on_collection_item_id", using: :btree
  add_index "enquiries", ["user_id"], name: "index_enquiries_on_user_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "sale_authorization_id"
    t.integer  "collection_item_id"
    t.integer  "address_id"
    t.integer  "user_id"
    t.integer  "quantity"
    t.string   "bra_size"
    t.string   "bust_size"
    t.string   "waist_size"
    t.string   "hip_size"
    t.text     "additional_info"
    t.string   "transaction_id"
    t.integer  "payment_cents"
    t.integer  "status",                default: 0
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "order_number"
    t.string   "credit_card"
  end

  add_index "orders", ["address_id"], name: "index_orders_on_address_id", using: :btree
  add_index "orders", ["collection_item_id"], name: "index_orders_on_collection_item_id", using: :btree
  add_index "orders", ["sale_authorization_id"], name: "index_orders_on_sale_authorization_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "responses", force: :cascade do |t|
    t.integer  "enquiry_id"
    t.text     "message"
    t.integer  "writer_id"
    t.string   "writer_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "responses", ["enquiry_id"], name: "index_responses_on_enquiry_id", using: :btree

  create_table "sale_authorizations", force: :cascade do |t|
    t.integer  "collection_item_id"
    t.integer  "enquiry_id"
    t.integer  "price_cents"
    t.integer  "status",             default: 0
    t.datetime "valid_to"
    t.string   "authorization_code"
    t.integer  "user_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "message"
  end

  add_index "sale_authorizations", ["authorization_code"], name: "index_sale_authorizations_on_authorization_code", using: :btree
  add_index "sale_authorizations", ["collection_item_id"], name: "index_sale_authorizations_on_collection_item_id", using: :btree
  add_index "sale_authorizations", ["enquiry_id"], name: "index_sale_authorizations_on_enquiry_id", using: :btree
  add_index "sale_authorizations", ["user_id"], name: "index_sale_authorizations_on_user_id", using: :btree

  create_table "transaction_errors", force: :cascade do |t|
    t.integer  "order_id"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "transaction_errors", ["order_id"], name: "index_transaction_errors_on_order_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "first_name",             default: "", null: false
    t.string   "last_name",              default: "", null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
