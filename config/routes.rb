# == Route Map
#
#                          Prefix Verb   URI Pattern                                                   Controller#Action
#                            root GET    /                                                             static_pages#home
#                    contact_page GET    /contact(.:format)                                            static_pages#contact
#                         contact POST   /contact(.:format)                                            static_pages#send_contact
#                      about_page GET    /about(.:format)                                              static_pages#about
#                new_user_session GET    /login(.:format)                                              users/sessions#new
#                    user_session POST   /login(.:format)                                              users/sessions#create
#            destroy_user_session DELETE /logout(.:format)                                             users/sessions#destroy
#                   user_password POST   /accounts/password(.:format)                                  devise/passwords#create
#               new_user_password GET    /accounts/password/new(.:format)                              devise/passwords#new
#              edit_user_password GET    /accounts/password/edit(.:format)                             devise/passwords#edit
#                                 PATCH  /accounts/password(.:format)                                  devise/passwords#update
#                                 PUT    /accounts/password(.:format)                                  devise/passwords#update
#        cancel_user_registration GET    /accounts/register/cancel(.:format)                           devise/registrations#cancel
#               user_registration POST   /accounts/register(.:format)                                  devise/registrations#create
#           new_user_registration GET    /accounts/register/sign_up(.:format)                          devise/registrations#new
#          edit_user_registration GET    /accounts/register/edit(.:format)                             devise/registrations#edit
#                                 PATCH  /accounts/register(.:format)                                  devise/registrations#update
#                                 PUT    /accounts/register(.:format)                                  devise/registrations#update
#                                 DELETE /accounts/register(.:format)                                  devise/registrations#destroy
#                     user_unlock POST   /accounts/unlock(.:format)                                    devise/unlocks#create
#                 new_user_unlock GET    /accounts/unlock/new(.:format)                                devise/unlocks#new
#                                 GET    /accounts/unlock(.:format)                                    devise/unlocks#show
#                 collection_item GET    /collections/:collection_id/:id(.:format)                     collection_items#show
#                     collections GET    /collections(.:format)                                        collections#index
#                      collection GET    /collections/:id(.:format)                                    collections#show
#                       enquiries POST   /enquiries(.:format)                                          enquiries#create
#         change_password_account POST   /account/change-password(.:format)                            accounts#change_password
#       account_enquiry_responses GET    /account/enquiries/:enquiry_id/responses(.:format)            responses#index
#                                 POST   /account/enquiries/:enquiry_id/responses(.:format)            responses#create
#               account_enquiries GET    /account/enquiries(.:format)                                  enquiries#index
#                 account_enquiry DELETE /account/enquiries/:id(.:format)                              enquiries#destroy
#     account_authorization_order POST   /account/authorizations/:authorization_id/order(.:format)     orders#create
# new_account_authorization_order GET    /account/authorizations/:authorization_id/order/new(.:format) orders#new
#          account_authorizations GET    /account/authorizations(.:format)                             authorizations#index
#           account_authorization DELETE /account/authorizations/:id(.:format)                         authorizations#destroy
#     account_order_retry_payment GET    /account/orders/:order_id/payment/retry(.:format)             payment#retry
# account_order_authorize_payment POST   /account/orders/:order_id/payment/authorize(.:format)         payment#authorize
#    account_order_payment_errors GET    /account/orders/:order_id/payment/errors(.:format)            payment#errors
#                  account_orders GET    /account/orders(.:format)                                     orders#index
#                   account_order GET    /account/orders/:id(.:format)                                 orders#show
#                         account GET    /account(.:format)                                            accounts#show
#                      refile_app        /attachments                                                  #<Refile::App app_file="/Users/royaltyclubvp/.rvm/gems/ruby-2.2.3/gems/refile-0.6.2/lib/refile/app.rb">
#

Rails.application.routes.draw do

  # Home Page
  root 'static_pages#home'

  # Contact Page
  get 'contact', to: 'static_pages#contact', as: :contact_page
  post 'contact', to: 'static_pages#send_contact', as: :contact

  # About Page
  get 'about', to: 'static_pages#about', as: :about_page

  # Authentication (Devise) Routes
  devise_for :users,
             path: '',
             path_names: {
                 'sign_in': 'login',
                 'sign_out': 'logout',
                 'password': 'accounts/password',
                 'unlock': 'accounts/unlock',
                 'registration': 'accounts/register',
                 'sign_up': 'sign_up'
             },
             controllers: {
                 sessions: 'users/sessions'
             }

  # Show Collection Item
  get 'collections/:collection_id/:id', to: 'collection_items#show', as: :collection_item

  # Show Collection
  resources :collections, only: [:show, :index]

  # Create Enquiry
  resources :enquiries, only: [:create]

  # Account Enquiries
  # Account Changes
  # Purchase Authorizations
  # Purchase Orders
  resource :account, only: [:show] do
    member do
      post 'change-password', to: 'accounts#change_password', as: :change_password
    end
    resources :enquiries, only: [:index, :destroy] do
      resources :responses, only: [:create, :index]
    end
    resources :authorizations, only: [:index, :destroy] do
      resource :order, only: [:new, :create]
    end
    resources :orders, only: [:index, :show] do
      get 'payment/retry', to: 'payment#retry', as: :retry_payment
      post 'payment/authorize', to: 'payment#authorize', as: :authorize_payment
      get 'payment/errors', to: 'payment#errors', as: :payment_errors
    end
  end
  get 'accounts/sale_authorizations', to: 'authorizations#index', as: :account_redirect
end
