if ENV['two_checkout_environment'] == 'sandbox'
  Twocheckout::API.credentials = {
      seller_id: ENV['two_checkout_sellerId'],
      private_key: ENV['two_checkout_privateKey'],
      sandbox: true
  }
elsif ENV['two_checkout_environment'] == 'production'
  Twocheckout::API.credentials = {
      seller_id: ENV['two_checkout_sellerId'],
      private_key: ENV['two_checkout_privateKey']
  }
end