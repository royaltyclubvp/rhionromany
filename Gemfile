source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.7.1'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Autoprefixer Gem
gem 'autoprefixer-rails'

# Include Bootstrap Framework
gem 'bootstrap', '~> 4.0.0.alpha1'

source 'https://rails-assets.org' do
  gem 'rails-assets-tether', '>= 1.1.0'
end

# Include Font Awesome
gem 'font-awesome-sass'

# Include Font Assets to Serve Fonts over CDN
gem 'font_assets'

# Use Refile as the upload solution
gem 'refile', require: 'refile/rails'
gem 'refile-mini_magick'
gem 'refile-s3'

# Use Devise for user authentication
gem 'devise'

# Use Friendly URLs
gem 'friendly_id', '~> 5.1.0'

# Use MetaMagic for MetaTag Generation
gem 'metamagic'

# Detect Time Zone automatically according to browser
gem 'browser-timezone-rails'

# Use for Country Select Forms
gem 'country_select', github: 'stefanpenner/country_select'

# Use Money Rails for Money Formatting & Autonumeric for Front End Money Formatting
gem 'money-rails'
gem 'autonumeric-rails'

# Use 2checkout for payment gateway
gem 'twocheckout'

# Annotate Files
gem 'annotate'

gem 'figaro'

# Analytics
gem 'google-analytics-rails', '1.1.0'

# Caching
# gem 'actionpack-page_caching'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :production do
  gem 'rails_12factor'
end


# Set PUMA as Rails Server
gem 'puma'
gem 'rack-timeout'

ruby '2.3.1'
