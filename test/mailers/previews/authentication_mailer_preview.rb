class AuthenticationMailerPreview < ActionMailer::Preview
  def confirmation_instructions
    AuthenticationMailer.confirmation_instructions(User.last, "faketoken", {})
  end

  def reset_password_instructions
    AuthenticationMailer.reset_password_instructions(User.last, "faketoken", {})
  end

  def unlock_instructions
    AuthenticationMailer.unlock_instructions(User.last, "faketoken", {})
  end
end