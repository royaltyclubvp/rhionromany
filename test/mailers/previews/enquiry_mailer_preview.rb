class EnquiryMailerPreview < ActionMailer::Preview
  def enquiry_made
    EnquiryMailer.enquiry_made(User.first, Enquiry.first)
  end
end